from conan import ConanFile

class RequirementsConan(ConanFile):
    name = "Requirements"

    def configure(self):
        self.options['stduuid'].with_cxx20_span = True
        self.options['stduuid'].with_system_generator = True
        
        self.options["spdlog"].no_exceptions = True
        self.options["tracy"].on_demand = True

        self.options["spirv-cross"].exceptions = False

        self.options["benchmark"].enable_exceptions = False
        self.options["benchmark"].enable_lto = True

    def requirements(self):
        self.requires("eastl/3.21.23")
        self.requires("unordered_dense/4.4.0")
        self.requires("mimalloc/2.1.7")
        self.requires("magic_enum/0.9.7")
        self.requires("stduuid/1.2.3")
        self.requires("sqlite3/3.47.1")
        self.requires("cli11/2.4.2")
        self.requires("xxhash/0.8.2")
        self.requires("effolkronium-random/1.5.0")
        self.requires("zeus_expected/1.2.0")

        self.requires("fmt/11.0.2")
        self.requires("spdlog/1.15.0")
        self.requires("tracy/0.11.1")
        self.requires("cpptrace/0.7.3")

        self.requires("glm/1.0.1")
        self.requires("lodepng/cci.20241228")
        self.requires("imgui/1.91.5-docking")

        self.requires("glfw/3.4")

        self.requires("vulkan-headers/1.3.239.0", override=True)
        self.requires("vulkan-validationlayers/1.3.239.0")
        self.requires("spirv-cross/1.3.239.0")
        self.requires("spirv-tools/1.3.239.0", override=True)
        self.requires("spirv-headers/1.3.239.0", override=True)
        self.requires("glslang/1.3.239.0", override=True)
        self.requires("vulkan-memory-allocator/cci.20231120")

        self.requires("gtest/1.15.0")
        self.requires("pixelmatch-cpp17/09102024")

        self.requires("shaderc/2024.1")
        self.requires("assimp/5.4.3")
        self.requires("meshoptimizer/0.22")
        self.requires("libsquish/1.15")
        
        self.requires("benchmark/1.9.0")
        self.requires("bshoshany-thread-pool/4.1.0")
        self.requires("entt/3.14.0")
        
