from conan import ConanFile

class RequirementsConan(ConanFile):
    name = "Requirements"

    def requirements(self):
        self.requires("qt/6.7.3")
